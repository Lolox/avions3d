Installation :
- Mettre le contenu dans un répertoire web
- Ouvrir le fichier index.html

Utilisation :
Sélectionner un pays pour voir les avions en provenance de ce pays, critere de sélection obligatoire à l'époque pour afficher les avions correctement sans trop de superpositions.

Bugs notoires :
Les avions sont parfois mal superposés sur l'axe z, en raison de leurs dimensions.
